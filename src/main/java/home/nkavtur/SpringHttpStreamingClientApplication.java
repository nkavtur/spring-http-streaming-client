package home.nkavtur;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.*;
import java.util.UUID;

@SpringBootApplication
public class SpringHttpStreamingClientApplication {
    public static final String URL = "http://localhost:8080/users/http-stream";
    public static final int CHUNK_SIZE = 128_000;

    public static void main(String[] args) {
        SpringApplication.run(SpringHttpStreamingClientApplication.class, args);
    }

    @Data
    @AllArgsConstructor
    public static class User {
        private UUID id;
        private String name;
    }

    @Autowired
    private ObjectMapper objectMapper;

    @Bean
    public CommandLineRunner commandLineRunner() throws IOException {
        return strings -> {
            try (CloseableHttpClient httpclient = HttpClients.createDefault()) {

                HttpGet get = new HttpGet(URL);
                httpclient.execute(get);

                try (CloseableHttpResponse response = httpclient.execute(get)) {
                    InputStream source = response.getEntity().getContent();
                    BufferedInputStream bufferedSource = new BufferedInputStream(source);
					byte[] readBuffer = new byte[CHUNK_SIZE];
					while (bufferedSource.read(readBuffer) != -1) {
						User user = objectMapper.readValue(readBuffer, User.class);
						System.out.println(user);
						readBuffer = new byte[CHUNK_SIZE];
					}
                }
            }
        };
    }
}
